<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class ExceptionSubscriber
 *
 * @package App\EventSubscriber
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * ExceptionSubscriber constructor.
     *
     * @param ContainerInterface   $container
     * @param LoggerInterface|null $logger
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger = null)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(KernelEvents::EXCEPTION => array('processException', 1));
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function processException(GetResponseForExceptionEvent $event)
    {
        if ($event->isMasterRequest() && $event->getException()->getCode() === 403) {
            $session = $this->container->get('session');
            if (!$session->getFlashBag()->has("exception")) {
                $session->getFlashBag()->add(
                    "exception",
                    "Une erreur s'est produite, veuillez contacter le support: webmaster@devfest"
                );
            }

            $url = $this->container->get('router')->generate('index', array(), UrlGeneratorInterface::ABSOLUTE_PATH);
            $response = new RedirectResponse($url, 302);
            $event->setResponse($response);

            $this->logger->critical('kernel.exception');
        }
    }
}
