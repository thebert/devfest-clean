<?php
/**
 * Created by PhpStorm.
 * User: nyavo
 * Date: 11/20/18
 * Time: 10:25 PM
 */
namespace App\EventSubscriber;

use App\Service\Browser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class RequestSubscriber
 *
 * @package App\EventSubscriber
 */
class RequestSubscriber implements EventSubscriberInterface
{
    private $container;

    /**
     * RequestSubscriber constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array('onKernelRequest', 1),
        );
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $browser = new Browser();
        $session = $this->container->get('session');

        if ($event->isMasterRequest() && $browser->getBrowser() != Browser::BROWSER_FIREFOX) {
            if (!$session->getFlashBag()->has("browser")) {
                $session->getFlashBag()->add('browser', "C'est une application destinée pour Firefox uniquement");
            }
        }
    }
}