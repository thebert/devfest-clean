<?php
/**
 * Created by PhpStorm.
 * User: nyavo
 * Date: 11/23/18
 * Time: 5:36 AM
 */
namespace App\EventSubscriber;

use App\Controller\TokenAuthenticatedInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class TokenSubscriber
 *
 * @package App\EventSubscriber
 */
class TokenSubscriber implements EventSubscriberInterface
{
    private $container;

    /**
     * @inheritDoc
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => ['onKernelController', 1],
            KernelEvents::RESPONSE => ['onKernelResponse', 1],
        ];
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }
        if ($controller[0] instanceof TokenAuthenticatedInterface) {
            $token = $event->getRequest()->get('token');
            if ($token != $this->container->getParameter('token')) {
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }
            $event->getRequest()->attributes->set('auth_token', $token);
        }
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        // check to see if onKernelController marked this as a token "auth'ed" request
        if (!$token = $event->getRequest()->attributes->get('auth_token')) {
            return;
        }

        $response = $event->getResponse();

        // create a hash and set it as a response header
        $hash = sha1($response->getContent().$token);
        $response->headers->set('X-CONTENT-HASH', $hash);
    }
}