<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ResponseSubscriber
 *
 * @package App\EventSubscriber
 */
class ResponseSubscriber implements EventSubscriberInterface
{
    /**
     * ResponseSubscriber constructor.
     *
     * @param LoggerInterface|null $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::RESPONSE => array('onKernelResponseAddCookie', 2),
        );
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponseAddCookie(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();
        if ($request->cookies->has('lastVisit')) {
            $content = $response->getContent();
            $content = preg_replace('/Welcome Admin!/', 'Welcome back Admin!', $content);
            $response->setContent($content);

            $this->logger->info('kernel.response: welcome back');
        } else {
            $datetime = new \DateTime();
            $cookie = new Cookie('lastVisit', $datetime->format('Y-m-d H:i:s'), new \DateTime('+30 days'));
            $response->headers->setCookie($cookie);

            $this->logger->info('kernel.response: welcome');
        }

        //$event->stopPropagation();
    }
}
