<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class TerminateSubscriber
 *
 * @package App\EventSubscriber
 */
class TerminateSubscriber implements EventSubscriberInterface
{
    public function __construct()
    {
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
    }

    /**
     * @param PostResponseEvent $event
     */
    public function onKernelTerminate(PostResponseEvent $event)
    {
    }
}
