<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/view", name="view")
     *
     * @Template("default/index.html.twig")
     *
     * @return array
     */
    public function viewAction()
    {
        return array();
    }

    /**
     * @Route("/error", name="error")
     *
     * @return Response
     */
    public function errorAction()
    {
        throw new AccessDeniedException();
    }

    /**
     * @Route("/form")
     *
     * @param \Swift_Mailer $mailer
     *
     * @return Response
     */
    public function adminAction(\Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('devfest.mada2018@gmail.com')
            ->setTo('')
            ->setBody(
                'Symfony kernel.terminate',
                'text/html'
            );

        $mailer->send($message);

        return $this->redirectToRoute("index");
    }
}
