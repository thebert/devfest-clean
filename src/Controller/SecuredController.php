<?php
/**
 * Created by PhpStorm.
 * User: nyavo
 * Date: 11/23/18
 * Time: 5:33 AM
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SecuredController
 *
 * @package App\Controller
 */
class SecuredController extends AbstractController implements TokenAuthenticatedInterface
{
    /**
     * @Route("/secured")
     */
    public function securedAction()
    {
        return $this->render('default/index.html.twig');
    }
}
